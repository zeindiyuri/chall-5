const express = require ('express');
const app = express();
const fs = require ('fs');
const data = require ('./users.json');

app.use(express.json());
app.use(express.urlencoded({extended:false}));
app.use(express.static("./public"));
app.set("view engine", "ejs");


app.get("/", (req,res) => {
    res.render ("index.ejs");
});


app.get("/games", (req,res) => {
    res.render ("game.ejs");
});


app.get("/login", (req,res) => {
    res.render ("login.ejs");
});

/////////////////// MAAF MAS LOGINNYA GAK BISA ////////////////////////////


app.use((req,res) => {
    res.status(404).render ("undefined.ejs");
});


app.listen (8000, () => console.log (`listening at http://localhost:${8000}`));




