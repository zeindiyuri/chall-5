const playerScore = document.getElementById("playerScore");

const computerScore = document.getElementById("computerScore");


const playerRock = document.getElementById("playerRock");
const playerPaper = document.getElementById("playerPaper");
const playerScissors = document.getElementById("playerScissors");

const computerRock = document.getElementById("computerRock");
const computerPaper = document.getElementById("computerPaper");
const computerScissors = document.getElementById("computerScissors");

const resultText = document.getElementById("resultText");

const allGameImg = document.querySelectorAll('img');

const selections = {
    Rock: { name:'Rock' , defeat:'Scissors'},
    Paper: { name:'Paper' , defeat:'Rock'},
    Scissors: { name:'Scissors' , defeat:'Paper'}
};

let computerSelect = '';
let playerScoreNumber = 0;
let computerScoreNumber = 0;

// update Score
function updateScore(playerSelect){
    console.log(playerSelect, computerSelect);
    const select = selections[playerSelect]
    if(playerSelect === computerSelect){
        resultText.textContent = "IMBANG";
    }
    else if(select.defeat.indexOf(computerSelect) > -1){
        resultText.textContent = "MENANG";
        playerScoreNumber++;
        playerScore.textContent = playerScoreNumber;

    }
    else{
        resultText.textContent = "KALAH";
        computerScoreNumber++;
        computerScore.textContent = computerScoreNumber;
    }
}

// Random Computer Choice
function computerRandomSelect(){
    const computerSelectNumber = Math.random();
   
    if(computerSelectNumber < 0.33){
        computerSelect = 'Rock';
    }
    else if(computerSelectNumber <= 0.70){
        computerSelect = 'Paper';
    }
    else{
        computerSelect = 'Scissors'
    }
    displayComputerSelect(computerSelect)
}

// passing Computer selection and styling buttons
function displayComputerSelect(computerSelect){

     // styling the computerSelection
     switch(computerSelect){
        case 'Rock':
            computerRock.classList.add('selected');
            break;
        case 'Paper':
            computerPaper.classList.add('selected');
            break;
        case 'Scissors':
            computerScissors.classList.add('selected');
            break;
        default:
            break;
    }
}

// Reset selected
function resetSelected(){
    allGameImg.forEach((img) => {
        img.classList.remove('selected')
    })
}


// passing player selection
function select(playerSelect){
    resetSelected()
 
    // styling the playerSelection
    switch(playerSelect){
        case 'Rock':
            playerRock.classList.add('selected');
            break;
        case 'Paper':
            playerPaper.classList.add('selected');
            break;
        case 'Scissors':
            playerScissors.classList.add('selected');       
            break;
        default:
            break;
    }
    computerRandomSelect();
    updateScore(playerSelect);
}